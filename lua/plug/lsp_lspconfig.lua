return {
    {
        "williamboman/mason.nvim",
        opts = {
            ui = {
                icons = {
                    package_installed = "✓",
                    package_pending = "➜",
                    package_uninstalled = "✗",
                },
            },
        },
        config = function(_, opts)
            require("mason").setup(opts)
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        event = "VeryLazy",
        dependencies = {
            { "williamboman/mason.nvim" },
            { "neovim/nvim-lspconfig" },
            { "folke/neodev.nvim" },
        },
        config = function()
            -- INFO: neodev
            require("neodev").setup()
            -- INFO: lspconfig
            local lspconfig = require("lspconfig")
            local mason_lspconfig = require("mason-lspconfig")
            -- setup mason-lspconfig
            mason_lspconfig.setup({
                ensure_installed = { "lua_ls", "vimls", "bashls" },
                automatic_installation = true,
            })
            -- setup lsps
            mason_lspconfig.setup_handlers({
                function(server_name)
                    lspconfig[server_name].setup({})
                end,
                ["lua_ls"] = function()
                    lspconfig.lua_ls.setup({
                        {
                            settings = {
                                Lua = {
                                    hint = { enable = true },
                                    runtime = { version = "LuaJIT" },
                                    workspace = {
                                        checkThirdParty = true,
                                        library = {
                                            [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                                            [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
                                        },
                                    },
                                    diagnostics = { globals = { "vim" } },
                                },
                            },
                        },
                    })
                end,
            })
            -- setup keymaps
            vim.api.nvim_create_autocmd("LspAttach", {
                callback = function(_)
                    -- local fn_format = function()
                        -- vim.lsp.buf.format({ async = true })
                    -- end
                    -- hover
                    vim.keymap.set("n", "K", vim.lsp.buf.hover, { desc = "hover (K)" })
                    -- vim.keymap.set("n", "<leader>lah", vim.lsp.buf.hover, { desc = "hover (K)" })
                    -- format
                    -- vim.keymap.set({ "n", "v" }, "<leader>laf", fn_format, { desc = "formatting" })
                    -- rename
                    -- vim.keymap.set({ "n", "v" }, "<leader>lar", vim.lsp.buf.rename, { desc = "rename" })
                    -- code_action
                    -- vim.keymap.set({ "n", "v" }, "<leader>laa", vim.lsp.buf.code_action, { desc = "code_action" })

                    -- definition
                    -- vim.keymap.set("n", "<leader>lgd", vim.lsp.buf.definition, { desc = "definition" })
                    -- declaration
                    -- vim.keymap.set("n", "<leader>lgD", vim.lsp.buf.declaration, { desc = "declaration" })
                    -- implementation
                    -- vim.keymap.set("n", "<leader>lgi", vim.lsp.buf.implementation, { desc = "implementation" })
                    -- references
                    -- vim.keymap.set("n", "<leader>lgr", vim.lsp.buf.references, { desc = "references" })
                    -- type_definition
                    -- vim.keymap.set("n", "<leader>lgt", vim.lsp.buf.type_definition, { desc = "type_definition" })
                end,
            })
        end,
    },
}
